<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', function () {
	    return redirect('/tasks');
	});
	
	Route::get('/tasks', 'TaskController@index');
	Route::get('/task/add', 'TaskController@addTask');
	Route::get('/task/completed', 'TaskController@hideCompleted');
	Route::get('/task/edit/{id}', 'TaskController@editTask');
	Route::post('/task/save', 'TaskController@saveTask');
	Route::post('/task/delete/{id}', 'TaskController@deleteTask');
	Route::post('/task/complete/{id}', 'TaskController@completeTask');
});
