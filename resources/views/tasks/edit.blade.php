@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @include('errors')
                <div class="panel-heading">Edit task</div> 
                <div class="panel-body">
                    <form action="/task/save" method="post" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $task->id }}">
                        <div class="form-group">
                            <label for="title" class="control-label">Title</label>
                            <input type="text" name="title" class="form-control" id="title" value="{{ $task->title }}">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Description</label>
                            <textarea class="form-control"  name="description" id="description">{{ $task->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="time" class="control-label">Deadline</label>
                            <input type="date" name="date" class="form-control" id="date" value="{{ $task->date }}">
                            <input type="time" name="time" class="form-control" id="time" value="{{ $task->time }}">
                        </div>
                        <div class="form-group">
                            <label for="file" class="control-label">Current file</label>
                            <p>
                                @if($task->file_name )
                                    <a target="_blank" href="{{ $task->filePath() }}">{{ $task->file_name }}</a>
                                @else
                                    No file
                                @endif
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="file" class="control-label">New file</label>
                            <input type="file" name="file" class="form-control" id="file">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
