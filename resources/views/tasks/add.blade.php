@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @include('errors')
                <div class="panel-heading">Add task</div> 
                <div class="panel-body">
                    <form action="/task/save" method="post" enctype='multipart/form-data'>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="title" class="control-label">Title</label>
                            <input type="text" name="title" class="form-control" id="title">
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label">Description</label>
                            <textarea class="form-control"  name="description" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="time" class="control-label">Deadline</label>
                            <input type="date" name="date" class="form-control" id="date">
                            <input type="time" name="time" class="form-control" id="time">
                        </div>
                        <div class="form-group">
                            <label for="file" class="control-label">File</label>
                            <input type="file" name="file" class="form-control" id="file">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
