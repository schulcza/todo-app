@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('errors')

            <div class="row" style="margin-bottom: 10px;">
                <form action="/tasks" method="GET">
                    <div class="col-md-8">
                        <input type="text" name="q" class="form-control" value="{{ $keyword }}">
                    </div>
                    <div class="col-md-2">
                        <input type="submit" class="btn btn-info btn-block" value="Search">
                    </div>
                </form>
                <div class="col-md-2">
                    <a href="/tasks" class="btn btn-info btn-block">Clear</a>
                </div>

            </div>
            
            <div class="panel panel-default">

                

                <div class="panel-heading">Tasks
                    <span><a href="/task/completed">({{ $cooike == 'show' ? 'Hide' : 'Show' }} completed)</a></span>
                    <a href="/task/add" class="pull-right">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                </div>

                <div class="panel-body">
                    @foreach($tasks as $key => $task)
    
                        <div class="panel panel-default">
                            <div class="panel-heading" >
                                <h3 class="panel-title">
                                    <div class="row">
                                        <div class="col-md-8">
                                            {!! $task->title !!}
                                        </div>
                                        <div class="col-md-3">
                                            <span class="pull-right">{{ \Carbon\Carbon::parse($task->deadline)->diffForHumans() }}</span>
                                        </div>
                                        <div class="col-md-1">
                                            <a class="pull-right" href="/task/edit/{{ $task->id }}">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                            </a>
                                        </div>
                                    </div>
                                </h3>
                            </div>
                            <div class="panel-body">
                                {!! $task->description !!}
                                
                                @if($task->done == 0)
                                    <p>
                                        @if($task->file)
                                            <span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span>
                                            <a target="_blank" href="{{ $task->filePath() }}">{{ $task->file_name }}</a>
                                        @endif

                                        <form action="/task/complete/{{ $task->id }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-success pull-right" value="Complete">
                                        </form>

                                        <form action="/task/delete/{{ $task->id }}" method="POST">
                                            {{ csrf_field() }}
                                            <input type="submit" class="btn btn-danger pull-right" value="Delete" onclick="return confirm('Are you sure to delete this task?')">
                                        </form>
                                    </p>
                                @else
                                <p>
                                    <span class="pull-right">
                                        Completed on {{ $task->completed_at }}
                                    </span>
                                </p>
                                @endif
                            </div>
                        </div>

                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection
