<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{

	use SoftDeletes;

    public function filePath() {
        return '/uploads/tasks/' . $this->id . '/' . $this->file;
    }
}
