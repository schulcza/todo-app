<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;

class TaskController extends Controller
{
	/**
	 * Show the tasks index page
	 */
	
    public function index(Request $request){

    	$cooike = $request->cookie('completed');
    	$keyword = $request->q;

    	$tasks = Task::where('user_id', \Auth::user()->id)
    					->orderBy('done', 'asc')
    					->orderBy('deadline', 'asc');

    	if ($cooike == 'hide') {
    		$tasks->where('done', 0);
    	}

    	if (isset($request->q) && $request->q != '') {
    		$tasks = $tasks->where('title', 'LIKE', '%' . $request->q . '%')
    						->orWhere('description', 'LIKE', '%' . $request->q . '%');
    	}

    	$tasks = $tasks->get();

    	foreach ($tasks as $key => $task) {
    		$task->title = str_replace($request->q, '<b><u>' . $request->q . '</u></b>', $task->title);
    		$task->description = str_replace($request->q, '<b><u>' . $request->q . '</u></b>', $task->description);
    	}
    	return view('tasks.index', compact('tasks', 'cooike', 'keyword'));
    }

    /**
	 * Save task to database and upload attachment
	 */
    public function saveTask(Request $request){

    	\Validator::make($request->all(), [
		    'title' => 'required|max:255',
		    'file' => 'file|mimes:pdf,xml,jpg|max:5128',
		    'date' => 'required',
		    'time' => 'required',
		])->validate();

    	$datetime = \Carbon\Carbon::parse($request->date . $request->time);

    	if (isset($request->id)) {
    		$task = Task::where('id', $request->id)->first();
    	} else {
    		$task = new Task;
    	}

    	if ($request->file('file')) {
	    	$file      = $request->file('file');
	        $fileName   = uniqid() . '.' . $file->getClientOriginalExtension();
    		\Storage::disk('uploads')->put('/tasks/' . $task->id . '/' . $fileName, file_get_contents($file), 'public');
	        
    		$task->file = $fileName;
    		$task->file_name = $file->getClientOriginalName();
    	}

    	$task->title = $request->title;
    	$task->description = $request->description;
    	$task->deadline = $datetime;
    	$task->user_id = \Auth::user()->id;

    	$task->save();


    	if (isset($request->id)) {
    		return redirect()->back()->withSuccess('Task updated');
    	} else {
    		return redirect()->back()->withSuccess('Task saved');
    	}

    }

    /**
	 * Render the add task page
	 */
    public function addTask(Request $request){
    	return view('tasks.add');
    }

    /**
	 * Get the task and render edit page
	 */
    public function editTask($taskId){
    	$task = Task::where('id', $taskId)->first();
    	$task->date = \Carbon\Carbon::parse($task->deadline)->format('Y-m-d');
    	$task->time = \Carbon\Carbon::parse($task->deadline)->format('H:i');

    	return view('tasks.edit', compact('task'));
    }

    /**
	 * Delete task
	 */
    public function deleteTask(Request $request){
    	$task = Task::where('id', $request->id)->first();
    	if ($task) {
    		$task->delete();
    		return redirect()->back()->withSuccess('Task delete successful');
    	}

    	return redirect()->back()->withErrors('Task not found');
    }

    /**
	 * Mark a task as complete
	 */
    public function completeTask(Request $request){
    	$task = Task::where('id', $request->id)->first();
    	if ($task) {
    		$task->done = 1;
    		$task->completed_at = \Carbon\Carbon::now();
    		$task->save();

    		return redirect()->back()->withSuccess('Task completed successful');
    	}

    	return redirect()->back()->withErrors('Task not found');

    }

    /**
	 * Hide/show completed task on index page
	 */
    public function hideCompleted(Request $request){
    	if ($request->cookie('completed') == 'show') {
    		$cookie = cookie('completed', 'hide');
    	} else {
    		$cookie = cookie('completed', 'show');    		
    	}

		return redirect('/tasks')->cookie($cookie);
    }
}
